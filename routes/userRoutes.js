const express = require("express");
const router = express.Router();
const auth = require("../auth");

const userController = require("../controllers/userControllers");

// Route for getting all users (Admin Only)
router.get("/", auth.verify, (request, response) => {

	const data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	};

	userController.getAllUsers(data).then((resultFromController) => {
		response.send(resultFromController);
	});
});

// Route for registering a user
router.post("/register", (request, response) => {
	userController.registerUser(request.body).then((resultFromController) => {
		response.send(resultFromController)
	});
});

// Route for logging into the system
router.post("/login", (request, response) => {
	userController.loginUser(request.body).then((resultFromController) => {
		response.send(resultFromController);
	});
});

// Route for retrieving a user's details
router.get("/details",auth.verify,(req,res)=>{

	const userData = auth.decode(req.headers.authorization);

	userController.getUser({userId:userData.id}).then(resultFromController=>res.send(resultFromController));
});


// Route for retrieving a specific user's details
router.get("/:userId", auth.verify, (request, response) => {	
	userController.getUser(request.params).then((resultFromController) => {
		response.send(resultFromController);
	});
});

// Route for retrieving a user's orders
router.get("/:userId/viewOrders", (request, response) => {	

	data = {
		userId: auth.decode(request.headers.authorization).id
	}

	userController.getMyOrders(request.params, data).then((resultFromController) => {
		response.send(resultFromController);
	});
});

router.get("/:userId/viewCart", (request, response) => {	

	data = {
		userId: auth.decode(request.headers.authorization).id
	}

	userController.getMyCart(request.params, data).then((resultFromController) => {
		response.send(resultFromController);
	});
});

// Route for setting admin role status
router.put("/:userId/setRole", auth.verify, (request, response) => {

	const data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	userController.setRole(request.params, data).then((resultFromController) => {
		response.send(resultFromController);
	});
});

// Route for adding a product to user's cart
// router.post("/:productId/addToCart", auth.verify, (request, response) => {
// 	const data = {
// 		userId: auth.decode(request.headers.authorization),
// 		product: request.body
// 	};

// 	productController.addToCart(request.params, data)
// 	.then((resultFromController) => {
// 		response.send(resultFromController)
// 	});
// });

module.exports = router;