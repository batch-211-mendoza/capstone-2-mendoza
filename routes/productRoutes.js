const express = require("express");
const router = express.Router();
const auth = require("../auth");

const productController = require("../controllers/productControllers");

// Route for adding a product
router.post("/addProduct", auth.verify, (request, response) => {
	const data = {
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	};

	productController.addProduct(data)
	.then((resultFromController) => {
		response.send(resultFromController)
	});
});


// Route for retrieving active products
router.get("/", (request, response) => {
	productController.getAllActiveProducts().then((resultFromController) => {
		response.send(resultFromController);
	});
});

// Route for retrieving all products
router.get("/all", (request, response) => {
	productController.getAllProducts().then((resultFromController) => {
		response.send(resultFromController);
	});
});

// Route for retrieving archived products
router.get("/archived", (request, response) => {
	productController.getAllArchivedProducts().then((resultFromController) => {
		response.send(resultFromController);
	});
});

// Route for retrieving a specific product
router.get("/:productId", (request, response) => {
	productController.getProduct(request.params).then((resultFromController) => {
		response.send(resultFromController);
	});
});

// Route for updating a product
router.put("/:productId/update", auth.verify, (request, response) => {

	const data = {
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	};

	productController.updateProduct(request.params, data).then((resultFromController) => {
		response.send(resultFromController);
	});
});

// Route for archiving a product
router.put("/:productId/archive", auth.verify, (request, response) => {

	const data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	productController.archiveProduct(request.params, data).then((resultFromController) => {
		response.send(resultFromController);
	});
});

// Route for activating a product
router.put("/:productId/activate", auth.verify, (request, response) => {

	const data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	};
	
	productController.activateProduct(request.params, data).then((resultFromController) => {
		response.send(resultFromController);
	});
});

// Route for adding a product to user's cart
router.post("/:productId/createOrder", auth.verify, (request, response) => {
	const data = {
		userId: auth.decode(request.headers.authorization),
		product: request.body
	};

	productController.createOrder(request.params, data)
	.then((resultFromController) => {
		response.send(resultFromController)
	});
});

module.exports = router;