const express = require("express");
const router = express.Router();
const auth = require("../auth");

const orderController = require("../controllers/orderControllers");

// Route for retrieving all orders
router.get("/", auth.verify, (request, response) => {

	const data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	};

	orderController.getAllOrders(data).then((resultFromController) => {
		response.send(resultFromController);
	});
});

module.exports = router;

