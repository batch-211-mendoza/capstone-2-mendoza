const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Product name cannot be blank."]
	},
	description: {
		type: String,
		required: [true, "Product description is required."]
	},
	price: {
		type: Number,
		required: [true, "Initial product price must be set."]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	orders: [
		{
			orderId: {
				type: String,
				required: [true, "orderId is required."]
			},
			quantity: {
				type: Number,
				required: [true, "quantity is required."]
			}
		}

	]
});

module.exports = mongoose.model("Product", productSchema);
