const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name cannot be blank."]
	},
	lastName: {
		type: String,
		required: [true, "Last name cannot be blank."]
	},
	email: {
		type: String,
		required: [true, "Email is required."]
	},
	password: {
		type: String,
		required: [true, "Password is required."]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile number is required."]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
});

module.exports = mongoose.model("User", userSchema);