const bcrypt = require("bcrypt");
const auth = require("../auth");
const User = require("../models/User");
const Order = require("../models/Order");


module.exports.registerUser = (requestBody) => {
	return User.find({email: requestBody.email}).then((result) => {
		if(result.length > 0){
			return false;
		} else {
			let newUser = new User({
				firstName: capitalizeAllFirst(requestBody.firstName).trim(),
				lastName: capitalizeAllFirst(requestBody.lastName).trim(),
				email: requestBody.email.toLowerCase().trim(),
				password: bcrypt.hashSync(requestBody.password,10),
				mobileNo: requestBody.mobileNo,
			});

			return newUser.save().then((user, error) => {
				if(error){
					return false;
				} else{
					return true;
				}
			});
		}
	});
}

module.exports.loginUser = (requestBody) => {
	return User.findOne({email: requestBody.email}).then((result) => {
		if(result == null){
			return false;
		} else{
			const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password);

			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result)};
			} else{
				return false;
			}
		}
	});
}

module.exports.getAllUsers = (data) => {
	if(data.isAdmin){
		return User.find({}).then((result) => {
			return result;
		});
	} else{
		return false;
	}
}

module.exports.getUser = (requestParams) => {
	return User.findById(requestParams.userId).then((result) => {
		result.password = "";
		return result;
	});
}

module.exports.setRole = (requestParams, data) => {
	if(data.isAdmin){
		return User.findById(requestParams.userId).then((result)=>{
			if(result.isAdmin !== true){
				return User.findByIdAndUpdate(requestParams.userId, {isAdmin: true}).then((result, error) => {
					if(error){
						return false;
					} else{
						return true ;
					}
				});	
			} else{
				return User.findByIdAndUpdate(requestParams.userId, {isAdmin: false}).then((result, error) => {
					if(error){
						return false;
					} else{
						return true ;
					}
				});
			}
		});
	} else {
		return Promise.resolve("Not authorized to submit the request.");
	}
}

module.exports.getMyOrders = (requestParams, data) => {
	if(data.userId === requestParams.userId){
		return Order.find({userId: requestParams.userId}).then((result) => {
			return result;
		});
	} else{
		return false;
	}
}

module.exports.getMyCart = (requestParams, data) => {
	if(data.userId === requestParams.userId){
		return Order.find({userId: requestParams.userId}).then((result1) => {
			return Order.find({cartState: "Idle"}).then((result2) => {
				return result2;
			})
		});
	} else{
		return false;
	}
}


// ----- Global ----- //

// Capitalization of the first letters of each word
function capitalizeAllFirst(string){
    
    // To remove the extra spaces in between each word
    let regexPattern = /\s+/g;
	let removedSpaces = string.replace(regexPattern, " ");

    let pieces = removedSpaces.split(" ");

    for (let i = 0; i < pieces.length; i++){
        let word = pieces[i].charAt(0).toUpperCase();
        pieces[i] = word + pieces[i].substr(1).toLowerCase();
    }
    return pieces.join(" ");
}

// Failed Authorization
function denyAccess(){
	return Promise.resolve("Not authorized to submit the request.");
}